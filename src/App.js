import React from "react";
import Route from "./routes";

import "./styles.css";

import Header from "./components/Header";

function App() {
  return (
    <div className="App">
      <Header />
      <Route />
    </div>
  );
}

export default App;
